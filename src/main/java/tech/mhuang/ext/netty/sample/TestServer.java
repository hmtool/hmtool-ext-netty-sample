package tech.mhuang.ext.netty.sample;

import tech.mhuang.ext.netty.sample.server.TestNettyServer;
import tech.mhuang.ext.netty.server.MyNettyServer;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * 测试netty server服务
 *
 * @author mhuang
 * @since 1.0.0
 */
@Slf4j
public class TestServer {
    public static void main(String[] args) {
        log.info("==== begin loader config ====");
        MyNettyServer myNettyServer = new TestNettyServer();
        myNettyServer.bind(8181);
        log.info("==== end loader config ====");
    }
}