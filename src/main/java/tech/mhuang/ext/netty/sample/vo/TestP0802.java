package tech.mhuang.ext.netty.sample.vo;

import tech.mhuang.ext.netty.vo.BaseRespMessage;

import java.io.Serializable;

/**
 * 心跳回复
 *
 * @author mhuang
 * @since 1.0.0
 */
public class TestP0802 extends BaseRespMessage implements Serializable {

    public static final int P0802 = 0x0802;//心跳回复

    private static final long serialVersionUID = 1L;

    public TestP0802() {
        getHeader().setMsgId(P0802);
    }
}
